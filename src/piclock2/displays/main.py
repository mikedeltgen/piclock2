"""
Led Clock Display Module
"""

import asyncio
import json
import logging
from typing import Any, Callable, Iterable, Tuple

from ..displays.clock import clock_display
from ..displays.date import date_display
from ..displays.manager import CycleItem, Cycler
from ..objects import Color, Digit, Message, MessageKey, Pixel
from ..settings import Settings
from ..strip import DisplayStrip
from .temperature import sauna_temperature_display, temperature_display

LOG = logging.getLogger(__name__)


# needs to be changed to something else to turn off the display
def dummy_function(*args: Any, **kwargs: Any) -> list[Digit]:
    return []


class Display:
    """
    controls the display of the clock
    """

    def __init__(
        self,
        settings: Settings,
        strip: DisplayStrip,
        brightness_queue: asyncio.Queue[int],
        message_queue: asyncio.Queue[Message],
    ) -> None:
        super().__init__()
        self.running = True
        self.display_functions: Iterable[Tuple[Callable[..., list[Digit]], int]]
        self.brightness: asyncio.Queue[int] = brightness_queue
        self.message_queue: asyncio.Queue[Message] = message_queue
        self._brightness: int = 1000
        self.strip: DisplayStrip = strip
        self.digits: list[Digit] = []
        self.settings: Settings = settings
        self.cycler = Cycler()
        self.cycler_task: asyncio.Task[Any]
        self.fader_calls: int = 0
        self.last_function: Callable[[list[Digit], int], list[Digit]]

    def setup(self) -> None:
        """
        setup the display
        """
        LOG.debug("Setting up display")
        with open(self.settings.CHARACTER_MAP_FILE, "r") as fptr:
            character_map = json.load(fptr)
        with open(self.settings.PIXEL_MAP_FILE, "r") as fptr:
            pixel_map = json.load(fptr)
            for key, map in pixel_map.items():
                d = Digit(key, character_map, map)
                d.color = Color(0, 255, 0)
                self.digits.append(d)
        self.cycler.add(CycleItem("clock", clock_display, 5, 1))
        self.cycler.add(CycleItem("date", date_display, 2, 7))
        self.cycler.add(CycleItem("temperature", temperature_display, 3, 1))
        self.cycler_task = asyncio.create_task(self.cycler.run())
        LOG.debug("Display setup complete")

    def stop(self) -> None:
        self.running = False

    def _build_strip_pixels(self, digits: list[Digit]) -> list[Pixel]:
        if not self.strip:
            raise Exception("No display set")
        if not digits:
            raise Exception("No digits set")
        pixels: list[Pixel] = []
        for i in range(self.strip.size):  # type: ignore
            pixels.append(Pixel(i, Color(0, 0, 0)))
        for digit in digits:
            digit.brightness = self._brightness
            for pixel in digit.get_pixels():
                pixels[pixel.number] = pixel
        return pixels

    def _set_strip_pixels(self, pixels: list[Pixel]) -> None:
        if not self.strip:  # type: ignore # Protocol not available in python 3.7
            raise Exception("No display set")
        if len(pixels) != self.strip.size:  # type: ignore
            raise Exception("Pixel count mismatch")
        for pixel in pixels:
            self.strip.setPixelColor(pixel.number, pixel.color)  # type: ignore

    def _strip_off(self) -> None:
        for i in range(self.strip.size):  # type: ignore
            self.strip.setPixelColor(i, Color(0, 0, 0))  # type: ignore
        self.strip.show()

    def fader(
        self, source: list[Digit], destination: list[Digit], call_count: int
    ) -> list[Pixel]:
        progress: int = int(self._brightness / 500 * call_count)
        source_pixels: dict[int, list[Pixel]] = {}
        destination_pixels: dict[int, list[Pixel]] = {}
        for digit in source:
            for pixel in digit.get_pixels():
                source_pixels[pixel.number] = pixel
        for digit in destination:
            for pixel in digit.get_pixels():
                destination_pixels[pixel.number] = pixel
        for idx in range(self.strip.size):
            if idx in source_pixels:
                source = source_pixels[idx]
        pixels: list[Pixel] = []
        return pixels

    def brightness_control(self) -> None:
        try:
            self._brightness = self.brightness.get_nowait()
            if self._brightness < self.settings.MIN_DAYLIGHT:
                self._brightness = self.settings.MIN_DAYLIGHT
            LOG.debug("Brightness set to %d", self._brightness)
        except asyncio.QueueEmpty:
            pass

    def message_handler(self) -> None:
        try:
            message = self.message_queue.get_nowait()
            if message.key == MessageKey.TEMPERATURE:
                if message.value == "sauna":
                    self.cycler.add(
                        CycleItem(
                            "sauna_temperature", sauna_temperature_display, 1, 1
                        )
                    )
                else:
                    self.cycler.remove(
                        CycleItem(
                            "sauna_temperature", sauna_temperature_display, 1, 1
                        )
                    )

        except asyncio.QueueEmpty:
            pass

    async def run(self) -> None:
        LOG.info("Starting Display")
        if not self.strip:
            raise Exception("No display set")
        if not self.digits:
            raise Exception("No digits set")
        func = self.cycler.current.function
        last_function: Callable[[list[Digit], int], list[Digit]] = func
        fading = False
        source_digits: list[Digit] = []
        destination_digits: list[Digit] = []
        changing_pixels: list[Pixel] = []
        call_count: int = 0
        try:
            while self.running:
                if not fading:
                    func = self.cycler.current.function
                    if func != last_function:
                        LOG.debug("Switching to %s", func.__name__)
                        call_count = 0
                        source_digits = last_function(self.digits, call_count)
                        destination_digits = func(self.digits, call_count)
                        last_function = func
                    else:
                        call_count += 1
                        changing_pixels = self._build_strip_pixels(
                            func(self.digits, call_count)
                        )
                else:
                    changing_pixels = self.fader(
                        source=source_digits,
                        destination=destination_digits,
                        call_count=self.fader_calls,
                    )
                    self.fader_calls += 1
                    if self.fader_calls > 500:
                        fading = False
                        self.fader_calls = 0
                self._set_strip_pixels(changing_pixels)
                self.strip.show()
                self.brightness_control()
                await asyncio.sleep(0.001)
        except asyncio.CancelledError:
            self.cycler_task.cancel()
            LOG.info("Display got canceled.")
            self._strip_off()
            self.running = False
        except Exception as e:
            LOG.error("Error occurred in Display: %s", e)
        LOG.warning("Display stopped")


def build_pixels(digits: list[Digit]) -> list[Pixel]:
    pixels: list[Pixel] = []
    for digit in digits:
        for pixel in digit.get_pixels():
            pixels.append(pixel)
    return pixels
