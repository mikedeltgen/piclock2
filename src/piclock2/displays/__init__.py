"""
Piclock2 Display Module
"""

from ..objects import Color, Digit, Pixel
from .clock import clock_display
from .date import date_display
from .main import Display
from .temperature import temperature_display

try:
    from rpi_ws281x import PixelStrip as Strip  # type:ignore
except (ModuleNotFoundError, ImportError):
    from ..strip import NoStrip as Strip


__all__ = [
    "Display",
    "temperature_display",
    "clock_display",
    "date_display",
    "Pixel",
    "Digit",
    "Color",
    "Strip",
]
