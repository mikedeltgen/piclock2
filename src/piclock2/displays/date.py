"""
Date module for piclock2
"""

from datetime import datetime
from typing import List

from ..objects import Digit


def date_display(digits: List[Digit], call_count: int) -> List[Digit]:
    now = datetime.now()
    d = now.day
    m = now.month
    # digits[0].color = Color(255, 255, 255)
    # digits[1].color = Color(255, 255, 255)
    # digits[3].color = Color(255, 255, 255)
    # digits[4].color = Color(255, 255, 255)
    digits[0].set_digit(str(d // 10))
    digits[1].set_digit(str(d % 10))
    digits[3].set_digit(str(m // 10))
    digits[4].set_digit(str(m % 10))
    digits[2].set_digit(" ")
    return digits
