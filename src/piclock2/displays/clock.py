"""
Clock module
"""

import time
from datetime import datetime
from typing import List

from ..objects import Digit


def clock_display(digits: List[Digit], call_count: int) -> List[Digit]:
    if len(digits) != 5:
        raise Exception("Clock needs 5 digits")
    now = datetime.now()
    h = now.hour
    m = now.minute
    digits[0].set_digit(str(h // 10))
    digits[1].set_digit(str(h % 10))
    digits[3].set_digit(str(m // 10))
    digits[4].set_digit(str(m % 10))

    if time.monotonic() % 2 < 1:
        digits[2].set_digit(" ")
    else:
        digits[2].set_digit(":")
    return digits
