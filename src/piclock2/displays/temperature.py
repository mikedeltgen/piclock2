"""
Temperature display for piclock2
"""

import logging
import os
import re
from typing import List

from ..objects import Digit
from ..settings import Settings

LOG = logging.getLogger(__name__)

R_TEMPERATURE = re.compile(r".*t*=(?P<temp>-?\d{1,5})$")
last_temperature: int = 0
last_call: int = 0


def get_temperature():
    """
    get temperature from 1 wire file
    """
    source = Settings().temperature_1w
    filename: str = os.path.join(source, "w1_slave")
    if not os.path.isfile(filename):
        raise FileExistsError("File %s does not exist" % filename)
    temperature = None
    with open(filename, "r") as fp:
        data = fp.read().split("\n")

    for row in data:
        temperature = R_TEMPERATURE.match(row)
        if temperature:
            temperature = int(temperature.group("temp")) / 1000
            break
    if not temperature:
        raise ValueError("Error while reading temperature")
    return temperature


def temperature_display(digits: List[Digit], call_count: int) -> List[Digit]:
    global last_temperature
    temperature = last_temperature

    if call_count == 1 or call_count % 100 == 0:
        try:
            temperature = int(get_temperature())
            last_temperature = temperature
        except ValueError:
            LOG.critical("Error reading temperature.")

    if temperature < 0:
        digits[0].set_digit("-")
    else:
        digits[0].set_digit(" ")
    temperature = abs(temperature)
    if temperature // 10 == 0:
        digits[1].set_digit(str(temperature % 10))
        digits[3].set_digit("C")
        digits[4].set_digit(" ")
    else:
        digits[1].set_digit(str(temperature // 10))
        digits[3].set_digit(str(temperature % 10))
        digits[4].set_digit("C")
    digits[2].set_digit(" ")

    return digits


def sauna_temperature_display(
    digits: List[Digit], value: int = 0
) -> List[Digit]:

    # we set a `2` in the first digit to indicate that this is the sauna
    # temperature
    digits[0].set_digit("2")
    temperature = abs(value)
    if temperature // 10 == 0:
        digits[1].set_digit(str(temperature % 10))
        digits[3].set_digit("C")
    else:
        digits[1].set_digit(str(temperature // 10))
        digits[3].set_digit(str(temperature % 10))
        digits[4].set_digit("C")
    digits[2].set_digit(" ")

    return digits
