import asyncio
import logging
from collections import deque
from time import monotonic
from typing import Any, Callable

from gouge.colourcli import Simple

from piclock2.objects import Digit

LOG = logging.getLogger(__name__)
Simple.basicConfig(level=logging.DEBUG)


class CycleItem:
    def __init__(
        self,
        name: str,
        function: Callable[[list[Digit], int], list[Digit]],
        interval: int,
        run_each: int = 1,
    ):
        self.name = name  # the name of the function
        self.function: Callable[[list[Digit], int], list[Digit]] = (
            function  # the function to run
        )
        self.interval = (
            interval  # the time in seconds that the function should run
        )
        self.run_each = (
            run_each  # the cycle number that the function should run at
        )
        self.last_run: float = 0  # the last time the function was run


def dummy_function(*args: Any, **kwargs: Any) -> list[Digit]:
    return []


class Cycler:
    """
    Cycles a list of functions
    """

    def __init__(self):
        self.items: list[CycleItem] = []
        self.current = CycleItem("dummy", dummy_function, 1)
        self.cycle_count: int = 0
        self.cycle_map: deque[CycleItem] = deque()
        self.max_cycles: int = 0
        self.next_cycle: int = 0

    def add(self, item: CycleItem):
        self.items.append(item)
        self.max_cycles = max([x.run_each for x in self.items])

    def remove(self, item: CycleItem):
        self.items.remove(item)
        self.max_cycles = max([x.run_each for x in self.items])

    def build_cycle_map(self) -> deque[CycleItem]:
        ret: list[CycleItem] = []
        items = sorted(self.items, key=lambda x: (x.run_each, x.name))

        for i in range(1, self.max_cycles + 1):
            res = None
            for item in items:
                if i == item.run_each:
                    res = item
                if not res:
                    if i % item.run_each == 0:
                        res = item
                if res:
                    ret.append(res)
        if ret:
            return deque(ret)
        return deque([CycleItem("dummy", dummy_function, 2, 1)])

    def distribute_items(self) -> deque[CycleItem]:
        result: list[CycleItem] = []
        next_indexes = {item: item.run_each - 1 for item in self.items}

        # Determine the maximum index to reach based on the highest
        # run_each value
        max_index = max(next_indexes.values())

        # Fill the list by checking the next valid position for each item
        while len(result) <= max_index:
            added = False
            for item in self.items:
                initial_interval = item.run_each
                if len(result) >= next_indexes[item]:
                    result.append(item)
                    next_indexes[item] = len(result) + initial_interval - 1
                    added = True
            if not added:
                break
        return deque(result)

    async def run(self):
        LOG.info("Starting cycler")
        try:
            while True:
                now = monotonic()
                if now - self.current.last_run > self.current.interval:
                    self.cycle_count += 1
                    if self.cycle_count % self.max_cycles == 0:
                        self.cycle_count = 1
                    if not self.cycle_map:
                        self.cycle_map = self.distribute_items()

                    if self.cycle_count >= self.next_cycle:
                        self.current = self.cycle_map.popleft()
                        LOG.info("Running %s", self.current.name)
                        self.next_cycle = 1
                        self.current.last_run = now
                await asyncio.sleep(0.001)
        except asyncio.CancelledError:
            LOG.info("Cycler cancelled")
        except Exception as e:
            LOG.exception(e)

        LOG.info("Cycler stopped")


async def main():
    cycler = Cycler()
    cycler.add(CycleItem("a", dummy_function, 1, 1))
    cycler.add(CycleItem("b", dummy_function, 2, 1))
    cycler.add(CycleItem("c", dummy_function, 1, 6))
    cycler.add(CycleItem("d", dummy_function, 1, 2))
    await cycler.run()


if __name__ == "__main__":
    asyncio.run(main())
