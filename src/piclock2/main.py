import asyncio
import signal
from typing import Any

from piclock2.displays.main import Display
from piclock2.mqtt.main import Monitor
from piclock2.objects import Color, Message, MessageKey
from piclock2.settings import Settings
from piclock2.strip import DisplayStrip

try:
    from rpi_ws281x import PixelStrip as Strip  # type:ignore
except (ModuleNotFoundError, ImportError):
    from piclock2.strip import NoStrip as Strip
import logging

from gouge.colourcli import Simple

Simple.basicConfig(level=logging.INFO)
LOG = logging.getLogger()
mqtt = logging.getLogger("paho.mqtt.client")
mqtt.setLevel(logging.INFO)
# handler = TimedRotatingFileHandler(
#     "piclock.log", when="midnight", backupCount=3
# )
# handler.setFormatter(Simple())
# handler.setLevel(logging.DEBUG)
# LOG.addHandler(handler)


def setup_strip(settings: Settings) -> DisplayStrip:
    strip: DisplayStrip = Strip(  # type:ignore
        settings.LED_COUNT,
        settings.LED_PIN,
        settings.LED_FREQ_HZ,
        settings.LED_DMA,
        settings.LED_INVERT,
        settings.LED_BRIGHTNESS,
        settings.LED_CHANNEL,
    )
    strip.begin()  # type:ignore
    return strip  # type:ignore


async def main():
    tasks: set[asyncio.Task[Any]] = set()
    settings = Settings()
    receive_queue: asyncio.Queue[Message] = asyncio.Queue()
    brightness_queue: asyncio.Queue[int] = asyncio.Queue()
    message_queue: asyncio.Queue[Message] = asyncio.Queue()
    running = True
    last_brightness: int = 100
    brightness_queue.put_nowait(last_brightness)

    def signal_handler():
        nonlocal running
        for task in tasks:
            task.cancel()
        running = False

    loop = asyncio.get_running_loop()
    loop.add_signal_handler(signal.SIGINT, signal_handler)

    def handle_task(task: asyncio.Task[Any]) -> None:
        """
        handle task callback.
        """
        name = task.get_name()
        LOG.info(f"Handling Task end. [{name}]")
        tasks.discard(task)
        if task.exception():
            if task.get_name() == "monitor":
                start_monitor()

    def start_monitor():
        monitor = Monitor(settings, receive_queue)
        monitor.setup()
        monitor_task = asyncio.create_task(monitor.run(), name="monitor")
        monitor_task.add_done_callback(handle_task)
        tasks.add(monitor_task)

    start_monitor()

    strip = setup_strip(settings)

    def start_display():
        for task in tasks:
            if task.get_name() == "display":
                LOG.debug("Display already running.")
                return
        display = Display(
            settings=settings,
            strip=strip,
            brightness_queue=brightness_queue,
            message_queue=message_queue,
        )
        display.setup()
        for item in display.digits:
            item.color = Color(255, 255, 255)
        display_task = asyncio.create_task(display.run(), name="display")
        display_task.add_done_callback(handle_task)
        tasks.add(display_task)

    start_display()

    try:
        while running:
            try:
                message = receive_queue.get_nowait()
                LOG.debug(f"{message.key}, {message.value}")

                if message.key == MessageKey.DISPLAY:
                    if message.value == "OFF":
                        for task in tasks:
                            name = task.get_name()
                            if name == "display":
                                task.cancel()
                    elif message.value == "ON":
                        brightness_queue.put_nowait(last_brightness)
                        start_display()
                elif message.key == MessageKey.DAYLIGHT:
                    try:
                        last_brightness = int(float(message.value))
                        brightness_queue.put_nowait(last_brightness)
                    except Exception:
                        pass
                else:
                    pass
                message_queue.put_nowait(message)
            except asyncio.QueueEmpty:
                pass
            await asyncio.sleep(0.01)
    except Exception as e:
        LOG.exception(e)
        LOG.info("Stopping Piclock.")
    finally:
        for task in tasks:
            task.cancel()
        await asyncio.gather(*tasks, return_exceptions=True)


if __name__ == "__main__":
    asyncio.run(main())
