import asyncio
import logging
import time
from typing import Any, List

import paho.mqtt.client as mqtt

from piclock2.objects import Message, MessageKey

from ..excep import MonitorRunTimeError
from ..settings import Settings

LOG = logging.getLogger(__name__)


class Monitor:
    """ """

    def __init__(
        self, settings: Settings, queue: asyncio.Queue[Message]
    ) -> None:
        self.settings = settings
        self.receive_queue = queue
        self._client: mqtt.Client
        self.running = False
        self.topic_prefix = settings.openhab_prefix
        self.last_on_message: float = time.monotonic() + 25
        self.sub_topics: List[str] = [
            "DAYLIGHT",
            "SUNSET",
            "SUNRISE",
            "DISPLAY",
        ]

    def setup(self):
        """
        initialize the MQTT client with hostname and port. Register on_message
        callback. Subscribe to the sauna loopback topic.
        """
        LOG.info("Setting up MQTT Monitor")
        self.client = mqtt.Client()
        self.client.enable_logger()

        self.client.on_message = self._on_msg
        LOG.debug(
            "MQTT Server: "
            f"{self.settings.mqtt_server}/{self.settings.mqtt_port}"
        )
        LOG.info("MQTT Monitor setup complete")

    def _disconnect(self, client: mqtt.Client, obj: Any, reason_code: int):
        pass

    def _on_msg(self, client: mqtt.Client, obj: Any, msg: mqtt.MQTTMessage):
        """
        callback if message received
        """
        data: str = msg.payload.decode("UTF-8")
        topic = msg.topic
        for item in self.sub_topics:
            if topic == f"{self.settings.openhab_prefix}/{item}":
                try:
                    message = Message(MessageKey(item), topic, data)
                    self.receive_queue.put_nowait(message)
                except Exception as e:
                    LOG.error(f"Error: {e}")
        self.last_on_message = time.monotonic()
        LOG.debug(f"Topic:{topic} -> {data}")

    def check_connection(self):
        """
        check if the last on_message is older than 1 second and publish the
        message
        """
        if self.last_on_message + 7 < time.monotonic():
            raise MonitorRunTimeError("No message received")
        self.last_on_message = time.monotonic()

    def set_last_will(self):
        """
        set the last will message
        need to disconnect and reconnect to set the last will message
        """
        env = ""
        if self.settings.environment:
            env = f"{self.settings.environment}"
        if self.client.is_connected():
            self.client.disconnect()

        self.client.will_set(
            f"PICLOCK/{env.upper()}/STATUS",
            "OFFLINE",
            0,
            False,
        )
        self.client.connect(
            self.settings.mqtt_server,
            self.settings.mqtt_port,
            self.settings.mqtt_timeout,
        )

    def stop(self):
        """
        stop the monitor
        """
        LOG.info("Stopping Monitor")
        self.running = False
        LOG.info("Monitor stopped try")

    async def run(self):
        """
        start the monitor
        """
        LOG.info("Starting Monitor")
        self.set_last_will()
        self.client.loop_start()
        for topic in self.sub_topics:
            self.client.subscribe(self.topic_prefix + "/" + topic)

        self.client.subscribe(f"PICLOCK/{self.settings.environment}/STATUS")
        self.client.publish(
            f"PICLOCK/{self.settings.environment}/DISPLAY", "ON"
        )
        self.running = True
        LOG.info("Monitor started")
        error_count = 0
        while self.running:
            try:
                self.client.publish(
                    f"PICLOCK/{self.settings.environment}/STATUS", "ONLINE"
                )
                await asyncio.sleep(5)
                self.check_connection()
            except MonitorRunTimeError:
                error_count += 1
                LOG.error("No message received")
                if error_count > 10:
                    LOG.critical("Monitor killed.")
                    raise
            except asyncio.CancelledError:
                self.running = False
            except Exception as e:
                LOG.error("Error occurred in Monitor: %s", e)

        self.client.publish(
            f"PICLOCK/{self.settings.environment}/DISPLAY", "OFF"
        )
        self.client.loop_stop()
        self.client.disconnect()
        self.running = False
        LOG.info("Monitor stopped")
