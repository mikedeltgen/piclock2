"""
PIClock2 exceptions
"""


class PIClock2Error(Exception):
    """Base class for PIClock2 exceptions"""


class NoDisplaySetError(PIClock2Error):
    """No display set"""


class MonitorRunTimeError(PIClock2Error):
    """No message received"""
