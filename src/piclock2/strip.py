"""
strip controller module
"""

from typing import Any, Protocol

from blessings import Terminal  # type: ignore

from .objects import RGBW, Pixel


class DisplayStrip(Protocol):
    """
    Display Strip Protocol
    """

    def __init__(self, led_count: int = 550) -> None: ...

    @property
    def size(self) -> int: ...

    def show(self) -> None: ...

    def setPixelColor(self, pixel: int, color: RGBW) -> None: ...


class NoStrip:
    """
    Strip Less test class
    """

    def __init__(self, led_count: int = 550, *args: Any, **kwargs: Any) -> None:
        self.led_count = led_count
        self._pixels = [Pixel(i) for i in range(led_count + 1)]
        self.row = ""

    @property
    def size(self) -> int:
        return self.led_count

    def show(self) -> None:
        term = Terminal()
        row = 0
        with term.location(0, row):  # type: ignore
            for idx, pixel in enumerate(self._pixels):
                if idx % 50 == 0 and idx > 0:
                    row += 1
                    print("\n", end="")
                print(pixel, end="")

    def setPixelColor(self, pixel: int, color: RGBW) -> None:
        self._pixels[pixel]._color = color

    def begin(self) -> None:
        pass
