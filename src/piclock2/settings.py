from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_prefix="PICLOCK_")
    debug: bool = True
    log_file: str = "log/piclock.log"
    mqtt_port: int = 1883
    mqtt_server: str = "mqtt.io"
    mqtt_timeout: int = 60
    openhab_prefix: str = "OPENHAB"
    environment: str = "dev"
    temperature_1w: str = "/sys/bus/w1/devices/28-00000a3b3b4e"
    LED_COUNT: int = 550  # Number of LED pixels. 86 on the clock
    LED_PIN: int = 18  # GPIO pin connected to the pixels (18 uses PWM!).
    LED_FREQ_HZ: int = 800000  # LED signal frequency in hertz (usually 800khz)
    LED_DMA: int = 10  # DMA channel to use for generating signal (try 10)
    LED_BRIGHTNESS: int = 20  # Set to 0 for darkest and 255 for brightest
    LED_INVERT: int = (
        False  # True to invert the signal (when using NPN transistor level shift)
    )
    LED_CHANNEL: int = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53
    DIGIT_COUNT: int = 5  # Number of digits on the clock
    MIN_DAYLIGHT: int = 20  # Minimum daylight value
    PIXEL_MAP_FILE: str = (
        "/Users/mdeltgen/workspace/pyspace/piclock2/pixel_map.json"
    )
    CHARACTER_MAP_FILE: str = (
        "/Users/mdeltgen/workspace/pyspace/piclock2/character_set.json"
    )

    @property
    def env_prefix(self):
        env_prefix: str = ""
        if self.environment:
            env_prefix = f"{self.environment}/"
        return env_prefix
