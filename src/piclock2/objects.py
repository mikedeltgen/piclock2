"""
Piclock2 objects
"""

from enum import Enum
from typing import Any, Callable, List, TypeVar

from termcolor import colored


class RGBW(int):
    def __new__(cls, r: int, g: int = 0, b: int = 0, w: int = 0):
        if (g, b, w) == (0, 0, 0):
            return int.__new__(cls, r)
        else:
            return int.__new__(cls, (w << 24) | (r << 16) | (g << 8) | b)

    @property
    def r(self):
        return (self >> 16) & 0xFF

    @property
    def g(self):
        return (self >> 8) & 0xFF

    @property
    def b(self):
        return (self) & 0xFF

    @property
    def w(self):
        return (self >> 24) & 0xFF

    def as_string(self) -> str:
        r = self.r
        g = self.g
        b = self.b
        # Define the 16 colors available in termcolor
        term_colors = {
            "grey": (128, 128, 128),
            "red": (255, 0, 0),
            "green": (0, 255, 0),
            "yellow": (255, 255, 0),
            "blue": (0, 0, 255),
            "magenta": (255, 0, 255),
            "cyan": (0, 255, 255),
            "white": (255, 255, 255),
            "on_grey": (128, 128, 128),
            "on_red": (255, 0, 0),
            "on_green": (0, 255, 0),
            "on_yellow": (255, 255, 0),
            "on_blue": (0, 0, 255),
            "on_magenta": (255, 0, 255),
            "on_cyan": (0, 255, 255),
            "on_white": (255, 255, 255),
        }

        # Calculate the Euclidean distance between the input color and each termcolor
        distances = {
            color: ((r - rgb[0]) ** 2 + (g - rgb[1]) ** 2 + (b - rgb[2]) ** 2)
            ** 0.5
            for color, rgb in term_colors.items()
        }

        # Return the termcolor with the smallest distance to the input color
        return min(distances, key=distances.get)


def Color(red: int, green: int, blue: int, white: int = 0):
    """Convert the provided red, green, blue color to a 24-bit color value.
    Each color component should be a value 0-255 where 0 is the lowest intensity
    and 255 is the highest intensity.
    """
    return RGBW(red, green, blue, white)


class Pixel:
    """
    Pixel Class represents a single pixel on the strip
    """

    def __init__(
        self, number: int, color: RGBW = Color(0, 0, 0), brightness: int = 1000
    ) -> None:
        self.number = number
        self._color = color
        self.brightness = brightness

    @property
    def color(self) -> RGBW:
        brightness_factor = self.brightness / 1000
        return Color(
            int(self._color.r * brightness_factor),
            int(self._color.g * brightness_factor),
            int(self._color.b * brightness_factor),
        )

    def __eq__(self, __value: object) -> bool:
        if not isinstance(__value, Pixel):
            return False
        return self.number == __value.number

    def __str__(self) -> str:
        if self.color.as_string() == "black":
            return " "
        # due to missing Literal support in python 3.7
        return colored("#", self.color.as_string())  # type: ignore

    def fade(self, destination: "Pixel", i: int) -> None:
        self._color = Color(
            int((destination.color.r - self.color.r) * i / 500) + self.color.r,
            int((destination.color.g - self.color.g) * i / 500) + self.color.g,
            int((destination.color.b - self.color.b) * i / 500) + self.color.b,
        )


class Digit:
    """
    Digit Class represents a single digit on the display
    """

    def __init__(
        self,
        id: str,
        character_set: dict[str, list[str]],
        pixel_map: dict[str, List[int]],
    ) -> None:
        self.id = id
        self.on_segments: List[str] = []
        self.color = Color(0, 0, 0)
        self.char = "_"
        self.segments: dict[str, List[int]] = pixel_map
        self.character_set = character_set
        self.pixel_map = pixel_map
        self.brightness = 255

    def set_digit(self, inp: str):
        self.on_segments: List[str] = []
        char_set = self.character_set.get(inp, [])
        if char_set:
            self.char = inp
        for segment in char_set:
            self.on_segments += segment

    def get_pixels(self) -> List[Pixel]:
        pixels: List[Pixel] = []
        for segment, segment_pixels in self.segments.items():
            color = Color(0, 0, 0)
            if segment in self.on_segments:
                color = self.color
            for _ in segment_pixels:
                pixel = Pixel(_, color, self.brightness)
                pixels.append(pixel)
        return pixels


class MessageKey(Enum):
    DAYLIGHT = "DAYLIGHT"
    TEMPERATURE = "TEMPERATURE"
    HUMIDITY = "HUMIDITY"
    PRESSURE = "PRESSURE"
    DISPLAY = "DISPLAY"


class Message:
    def __init__(self, key: MessageKey, topic: str, value: Any) -> None:
        self.key: MessageKey = key
        self.topic: str = topic
        self.value: Any = value
