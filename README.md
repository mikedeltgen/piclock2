piclock2
========

`piclock2` is a 7 segment LED Clock with temperature display based on WS2812b 
led strip.


Functions
---------

- display the current time
- display the current temperature
- change color


Changelog
---------

-v1.2.1
    - add storing of last brightness

-v.1.2.0
    - downgrade to python 3.9 due to incompatibility on Pi Zero W

-v.1.1.0
    - add mqtt display control

-v.1.0.2
    - update async signal and error handling
    - update self monitoring

- v.1.0.1
    - Bug fix when w1_slave file could not be read
    - add call counter to prevent calling the temperature function too many
        times.

- v.1.0.0 

    - initial version
    