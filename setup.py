from setuptools import find_packages, setup

setup(
    name="piclock2",
    version="1.2.0",
    author="Mike Deltgen",
    author_email="mike@deltgen.net",
    description="Raspberry PI 7 Segment LED Clock based on ws2812b led strip.",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mikedeltgen/piclock2",
    packages=find_packages(where="src", include=["piclock2"]),
    package_dir={"": "src"},
    python_requires=">=3.9",
    classifiers=[
        "Environment :: Console",
        "Programming Language :: Python :: 3",
    ],
    install_requires=[
        "gouge",
        "blessings",
        "termcolor",
        "pydantic_settings",
        "paho.mqtt",
        "rpi-ws281x; sys_platform == 'linux'",
    ],
    extras_require={
        "dev": [
            "furo",
            "sphinx",
        ],
        "test": [
            # Add test dependencies here
        ],
    },
)
